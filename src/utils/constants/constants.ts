export const XLSXExten = '.xlsx';
export const DEFAULT_SHEET_NAME = 'Sheet 1';
export const MOMENT_FORMAT = 'DD/MM/YYYY HH:mm';