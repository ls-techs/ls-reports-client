import * as React from 'react';
import { dateInputTypes } from '../../../utils/constants/enums';

export interface IProps {
    filter: any;
    onChange: Function;
}
 
export interface IState {
}

export default class DateFilter extends React.Component<IProps, IState> {
    // organize the 3 inputs to one object that will be passed as filter value in across this column
    private onDateChange = (inputType: dateInputTypes, inputValue: any) => {
        const { filter, onChange } = this.props;
        let value;
        if (filter && filter.value) {
            value = filter.value;
        } else {
            value = {};
        }

        value[inputType] = inputValue;
        onChange(value);
    }

    renderFirstDateInput(filter: any) {
        if (filter && filter.value && filter.value[dateInputTypes.select] !== "All") {
            return (
                <input
                    type="date"
                    style={{
                        width: '100%',
                    }}
                    value={filter.value[dateInputTypes.firstDateInput] || ''}
                    onChange={event => this.onDateChange(dateInputTypes.firstDateInput, event.target.value)}
                />
            );
        }
        return null;
    }

    renderSecondDateInput(filter: any) {
        if (filter && filter.value && filter.value[dateInputTypes.select] === "Between") {
            return (
                <input
                    type="date"
                    style={{
                        width: '100%',
                    }}
                    value={filter.value[dateInputTypes.secondDateInput] || ''}
                    onChange={event => this.onDateChange(dateInputTypes.secondDateInput, event.target.value)}
                />
            );
        }
        return null;
    }

    render() {
        const { filter } = this.props;
        return (
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <select
                    onChange={event => this.onDateChange(dateInputTypes.select, event.target.value)}
                    style={{ width: "100%" }}
                    value={filter && filter.value ? filter.value[dateInputTypes.select] : "all"}
                >
                    <option value="All">Show All</option>
                    <option value="Equal">Equal</option>
                    <option value="Before">Before</option>
                    <option value="Before And Equal">Before And Equal</option>
                    <option value="After">After</option>
                    <option value="After And Equal">After And Equal</option>
                    <option value="Between">Between</option>
                </select>
                {this.renderFirstDateInput(filter)}
                {this.renderSecondDateInput(filter)}
            </div>
        );
    }
}