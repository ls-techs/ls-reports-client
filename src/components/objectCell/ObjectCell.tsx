import * as React from 'react'
import * as TreeView from 'react-treeview';
import "react-treeview/react-treeview.css";
import { booleanConfig, dateConfig, objectConfig } from '../../configs/configs';
import { HelperMethods } from '../../utils/helperMethods/HelperMethods';

export interface IProps {
	type: string;
	properties: any;
	filter: any;
	key?: any;
}

export default class ObjectCell extends React.Component<IProps, {}> {
	renderPropertyValue(value) {
		if (typeof value === "boolean") {
			return booleanConfig.Cell({ value });
		} else if (HelperMethods.isValidDate(value)) {
			return dateConfig.Cell({ value });
		}
		return value;
	}

	renderProprties(properties: object, filter: any) {
		return Object.keys(properties).map(k => {
			const value = properties[k];
			if (typeof value === "object" && !HelperMethods.isValidDate(value)) {
				return objectConfig.cell({ value }, k, k);
			}
			return <div key={k} className="info">{k}: {this.renderPropertyValue(value)}</div>;
		});
	}

	getKey(key: any) {
		if (key) {
			return { key };
		}
		return {}
	}

	render() {
		const { type, properties, key, filter } = this.props;
		const label = <span className="node">{type}</span>;
		if (properties) {
			return (
				<TreeView {...this.getKey(key)} nodeLabel={label} defaultCollapsed={false}>
					{this.renderProprties(properties, filter)}
				</TreeView>
			);
		}
		return null;
	}
}
