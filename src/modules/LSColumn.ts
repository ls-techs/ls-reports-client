import { dataTypes } from "../utils/constants/enums";
import { numberConifg, stringConifg, booleanConfig, dateConfig, objectConfig } from "../configs/configs";

export class LSColumn {
    Header: string;
	id: string;
	accessor: string;
    dataType: string;
    //filterMethod: Function;

    Filter?: Function;
    Cell?: Function;

    //object
    collapsed?: boolean;
    properties?: object;
    searchableProperties?: object;

    static parseData(schema: object[]) {
        return schema.map(sObject => new LSColumn(sObject))
    }
    
    constructor(schemaObject) {
        this.Header = schemaObject.Header;
        this.id = schemaObject.id;
        this.accessor = schemaObject.accessor;
        this.dataType = schemaObject.dataType;

        switch (this.dataType) {
            case dataTypes.number:
                (<any>Object).assign(this, numberConifg)
                break;
            
            case dataTypes.string:
                (<any>Object).assign(this, stringConifg)
                break;
            
            case dataTypes.boolean:
                (<any>Object).assign(this, booleanConfig)
                break;

            case dataTypes.date:
                (<any>Object).assign(this, dateConfig)
                break;
            
            case dataTypes.object:
                this.collapsed = schemaObject.collapsed;
                this.properties = schemaObject.properties;
                this.searchableProperties = schemaObject.searchableProperties;
                this.Filter = ({ filter, onChange }) => objectConfig.filter(filter, onChange, this.searchableProperties);
                this.Cell = filter => objectConfig.cell(filter, this.accessor);
                (<any>Object).assign(this, objectConfig)
                break;
            
            default:
                break;
        }
    }
}