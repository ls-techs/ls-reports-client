
import * as React from 'react';
import * as _ from 'lodash';
import * as moment from 'moment';
import { HelperMethods } from '../utils/helperMethods/HelperMethods';
import { dateInputTypes, dateFilterModes, numberInputTypes, numberFilterModes, objectInputTypes, dataTypes } from '../utils/constants/enums';
import DateFilter from '../components/Filters/dateFilter/DateFilter';
import NumberFilter from '../components/Filters/numberFilter/NumberFilter';
import ObjectCell from '../components/objectCell/ObjectCell';
import ObjectFilter from '../components/Filters/objectFilter/ObjectFilter';
import BooleanFilter from '../components/Filters/booleanFilter/BooleanFilter';

export const numberConifg = {
    filterMethod: (filter, rows) => {
        if (filter && filter.value) {
            const { value } = filter;

            const selectValue = value[numberInputTypes.select];
            const firstInputVal = value[numberInputTypes.firstNumberInput]
            const secondInputVal = value[numberInputTypes.secondNumberInput]

            if (selectValue !== numberFilterModes.Between && (firstInputVal === "" || !firstInputVal)) {
                return rows;
            } else if (selectValue === numberFilterModes.Between && (firstInputVal === "" || !firstInputVal) && (secondInputVal === "" || !secondInputVal)) {
                return rows;
            }

            const { readProp } = HelperMethods;

            switch (selectValue) {
                case numberFilterModes.Equal:
                    return rows.filter(r => readProp(r, filter.id) == firstInputVal);

                case numberFilterModes.LessThan:
                    return rows.filter(r => readProp(r, filter.id) < firstInputVal);

                case numberFilterModes.LessThanOrEqual:
                    return rows.filter(r => readProp(r, filter.id) <= firstInputVal);

                case numberFilterModes.GreaterThan:
                    return rows.filter(r => readProp(r, filter.id) > firstInputVal);

                case numberFilterModes.GreaterThanOrEqual:
                    return rows.filter(r => readProp(r, filter.id) >= firstInputVal);

                case numberFilterModes.Between:
                    return rows.filter(r => firstInputVal > readProp(r, filter.id) && r[filter.id] > secondInputVal);

                case dateFilterModes.All:
                default:
                    return rows;
            }
        }
        return rows;
    },
    Filter: ({ filter, onChange }): JSX.Element => <NumberFilter filter={filter} onChange={onChange} />,
    filterable: true,
    filterAll: true
}

export const stringConifg = {
    filterMethod: (filter, rows): object[] => {
        const { value } = filter;
        // if matches substring
        if (value) {
            const lowerCaseValue = value.toLowerCase()
            return rows.filter(r => {
                // the relevant string for filtering from the current row in filter
                const { readProp } = HelperMethods;
                const relString = readProp(r, filter.id);
                if (relString) {
                    return relString.toLowerCase().indexOf(lowerCaseValue) >= 0;
                }
                return false;
            })
        }
        return rows;
    },
    filterable: true,
    filterAll: true
}

// IDEA: in the schema we get from the server, also recive a string to replace a boolean 
// value of true, instead of always calling setting it to "True" (same for false)
export const booleanConfig = {
    Cell: filter => {
        const { value } = filter;
        if (value === true) {
            return "True";
        } else if (value === false) {
            return "False";
        }
        return "Undefined"
    },
    Filter: ({ filter, onChange }): JSX.Element => <BooleanFilter filter={filter} onChange={onChange} />,
    filterMethod: (filter, rows) => {
        if (filter.value === 'all') {
            return rows;

        } else if (rows && rows.length > 0) {
            const { readProp } = HelperMethods;
            switch (filter.value) {
                // get all rows with value true, this colimn value can be courrpted
                // that's way we have to check it against 'true' and 'false'
                case "True":
                    return rows.filter(r => readProp(r, filter.id) === true);
                
                case "False":
                    return rows.filter(r => readProp(r, filter.id) === false);
                
                case "All":
                default:
                    return rows;
            }
        }
        return [];
    },
    filterable: true,
    filterAll: true
}

// IDEA: receive moment formatting in the schema we get from the server
export const dateConfig = {
    Cell: filter => {
        const { value } = filter;
        if (HelperMethods.isValidDate(value)) {
            return moment(value).format('DD/MM/YYYY');
        }
        return "Invalid Date";
    },
    filterMethod: (filter, rows) => {
        if (filter && filter.value) {
            const { value } = filter;
            const { DateObjectToCompare, readProp } = HelperMethods;

            const selectValue = value[dateInputTypes.select];
            const firstInputVal = value[dateInputTypes.firstDateInput];
            const secondInputVal = value[dateInputTypes.secondDateInput];

            if (selectValue !== numberFilterModes.Between && (firstInputVal === "" || !firstInputVal)) {
                return rows;
            } else if (selectValue === numberFilterModes.Between && (firstInputVal === "" || !firstInputVal) && (secondInputVal === "" || !secondInputVal)) {
                return rows;
            }

            switch (selectValue) {
                case dateFilterModes.Equal:
                    return rows.filter(r => moment(DateObjectToCompare(readProp(r, filter.id))).isSame(moment(firstInputVal)));

                case dateFilterModes.Before:
                    return rows.filter(r => moment(DateObjectToCompare(readProp(r, filter.id))).isBefore(moment(firstInputVal)));

                case dateFilterModes.BeforeAndEqual:
                    return rows.filter(r => moment(DateObjectToCompare(readProp(r, filter.id))).isSameOrBefore(moment(firstInputVal)));

                case dateFilterModes.After:
                    return rows.filter(r => moment(DateObjectToCompare(readProp(r, filter.id))).isAfter(moment(firstInputVal)));

                case dateFilterModes.AfterAndEqual:
                    return rows.filter(r => moment(DateObjectToCompare(readProp(r, filter.id))).isSameOrAfter(moment(firstInputVal)));

                case dateFilterModes.Between:
                    // firstInputVal > x > second input val
                    return rows.filter(r => moment(DateObjectToCompare(readProp(r, filter.id))).isBetween(moment(secondInputVal), moment(firstInputVal)));

                case dateFilterModes.All:
                default:
                    return rows;
            }
        }
        return rows;
    },
    Filter: ({ filter, onChange }): JSX.Element => <DateFilter filter={filter} onChange={onChange} />,
    sortMethod: (a, b) => {
        if (!HelperMethods.isValidDate(a)) {
            return -1;
        } else if (!HelperMethods.isValidDate(b)) {
            return 1;
        }
        // both valid Dare objects by now
        a = a.getTime();
        b = b.getTime();
        return a > b ? 1 : -1;
    },
    filterable: true,
    filterAll: true
}

export const objectConfig = {
    cell: (filter: any, type: string, key?: any) => <ObjectCell filter={filter} type={type} properties={filter.value} />,
    filter: (filter, onChange, searchableProperties): JSX.Element => <ObjectFilter searchableProperties={searchableProperties} filter={filter} onChange={onChange} />,
    filterMethod: (filter, rows) => {
        const { propType, propName } = filter.value[objectInputTypes.select];
        const accessor = `${filter.id}.${propName}`;

        switch (propType) {
            case dataTypes.number:
                if (!filter.value.numberFilter) {
                    filter.value.numberFilter = {}
                }
                filter.value.numberFilter.id = accessor;
                return numberConifg.filterMethod(filter.value.numberFilter, rows);
            
            case dataTypes.string:
                if (!filter.value.stringFilter) {
                    filter.value.stringFilter = {}
                }
                filter.value.stringFilter.id = accessor;
                return stringConifg.filterMethod(filter.value.stringFilter, rows);
            
            case dataTypes.boolean:
                if (!filter.value.booleanFilter) {
                    filter.value.booleanFilter = {}
                }
                filter.value.booleanFilter.id = accessor;
                return booleanConfig.filterMethod(filter.value.booleanFilter, rows);
            
            case dataTypes.date:
                if (!filter.value.dateFilter) {
                    filter.value.dateFilter = {}
                }
                filter.value.dateFilter.id = accessor;
                return dateConfig.filterMethod(filter.value.dateFilter, rows);
            
            case dataTypes.object:
                if (!filter.value.objectFilter) {
                    return rows;
                }
                filter.value.objectFilter.id = accessor;
                return objectConfig.filterMethod(filter.value.objectFilter, rows);

            default:
                return rows;
        }
    },
    filterable: true,
    filterAll: true
}